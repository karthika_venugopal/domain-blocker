const {app,BrowserWindow}=require('electron');

function createWindow() 
{

    let blockerLoading=new BrowserWindow();
    blockerLoading.loadFile('Domain_blocker_Loading.html')

    blockerLoading.on('ready-to-show',()=>
    {
        blockerLoading.show();
        setTimeout(() => {
            blockerLoading.close();
            blockerWindow.show();
            
        },3000);
       
    });

    let blockerWindow=new BrowserWindow({
        webPreferences:{
            nodeIntegration:true,
            contextIsolation:false,
        },
        show:false
    });
    blockerWindow.loadFile("index.html");

    blockerWindow.on('close',()=>{
        blockerWindow=null;
    });
}

app.on('ready',createWindow);
